﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewController : MonoBehaviour {

    public Vector2 colliderSize = new Vector2();

    public Transform prefab;
    public Material PreviewMaterial;
    public Transform CreatePreview()
    {
        Transform obj = (Transform) Instantiate(prefab);
        obj.tag = "NoInvokable";
        obj.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        Destroy(obj.GetComponent<BoxCollider2D>());
        Destroy(obj.GetComponent<PolygonCollider2D>());
        Destroy(obj.GetComponent<CircleCollider2D>());

        foreach (var renderer in obj.GetComponentsInChildren<SpriteRenderer>(true))
            renderer.material = PreviewMaterial;
        // If the building / object has some scripts or other components
        // which shouldn't be on the preview, remove them here:
        foreach (var script in obj.GetComponentsInChildren<MonoBehaviour>(true))
            Destroy(script);
        return obj;
    }

    /*
    public Transform Place()
    {
        Transform obj = (Transform)Instantiate(prefab, transform.position, transform.rotation);
        Destroy(gameObject);
        return obj;
    }
    */
}
