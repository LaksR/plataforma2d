﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVolume : MonoBehaviour {
public Slider Volume;
public AudioSource Mymusic;

void Update () {
    Mymusic.volume = Volume.value;
}

}
