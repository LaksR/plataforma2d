﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pausemenu : MonoBehaviour {

    public static bool gamepause = false;

    public GameObject pausemenuUI;
	
	// Update is called once per frame
	void Update () {
		
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gamepause)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
	}

    public void Resume ()
    {
        pausemenuUI.SetActive(false);
        Time.timeScale = 1f;
        gamepause = false;
    }
    void Pause ()
    {
        pausemenuUI.SetActive(true);
        Time.timeScale = 0f;
        gamepause = true;
    }
    public void Loadmenu ()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    public void Quitgame ()
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }
}
