﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public GameObject scenario;
    public GameObject levelDesign;
    public AudioSource backgroundSound;
    public AudioSource deadSound;

    public Transform m_currMovingPlatform { get; private set; }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Dead" || collision.transform.tag == "Enemy")
        {

            backgroundSound.Pause();
            deadSound.Play();
            GetComponent<Rigidbody2D>().gravityScale = 0f;
            GetComponent<Transform>().position = new Vector3( transform.position.x, transform.position.y, transform.position.z );
            GetComponent<Transform>().localScale = new Vector3( 10, 10, 10 );
            scenario.SetActive(false);
            levelDesign.SetActive(false);
            StartCoroutine(restartGame());
        }
        if (collision.gameObject.tag == "MovingSurfaceCollider")
        {
            m_currMovingPlatform = collision.gameObject.transform;
            transform.SetParent(m_currMovingPlatform);
        }
        if (collision.gameObject.tag == "Win") {
            SceneManager.LoadScene("Menu");
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "MovingSurfaceCollider")
            m_currMovingPlatform = null;
        transform.SetParent(null);
    }

    IEnumerator restartGame(){
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Game");
    }

}